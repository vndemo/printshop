<?php
/**
 * This file represents an example of the code that themes would use to register
 * the required plugins.
 *
 * It is expected that theme authors would copy and paste this code into their
 * functions.php file, and amend to suit.
 *
 * @package    TGM-Plugin-Activation
 * @subpackage Example
 * @version    2.4.0
 * @author     Thomas Griffin <thomasgriffinmedia.com>
 * @author     Gary Jones <gamajo.com>
 * @copyright  Copyright (c) 2014, Thomas Griffin
 * @license    http://opensource.org/licenses/gpl-2.0.php GPL v2 or later
 * @link       https://github.com/thomasgriffin/TGM-Plugin-Activation
 */


require_once get_template_directory() . '/inc/tgm/class-tgm-plugin-activation.php';

add_action( 'tgmpa_register', 'register_required_plugins' );
/**
 * Register the required plugins for this theme.
 *
 * In this example, we register two plugins - one included with the TGMPA library
 * and one from the .org repo.
 *
 * The variable passed to tgmpa_register_plugins() should be an array of plugin
 * arrays.
 *
 * This function is hooked into tgmpa_init, which is fired within the
 * TGM_Plugin_Activation class constructor.
 */
function register_required_plugins() {

    /**
     * Array of plugin arrays. Required keys are name and slug.
     * If the source is NOT from the .org repo, then source is also required.
     */
    $plugins = array(
        array(
            'name'               => 'Siteorigin Panels Netbase',
            'slug'               => 'siteorigin-panels-netbase',
            'required'           => true,
            'force_activation'   => false,
            'force_deactivation' => false,
            'source'             => esc_url('http://demo9.cmsmart.net/plugins/printshop-solution/siteorigin-panels-netbase.zip'),
        ),

        array(
            'name'               => 'Redux Framework',
            'slug'               => 'redux-framework',
            'required'           => true,
            'force_activation'   => false,
            'force_deactivation' => false,
        ),
        
        array(
            'name'               => 'Slider Revolution',
            'slug'               => 'revslider',
            'source'             => esc_url('http://www.netbaseteam.com/wordpress/theme/plugins/revslider.zip'),
            'required'           => false,
            'force_activation'   => false,
            'force_deactivation' => false,
            'external_url'       => 'http://codecanyon.net/item/slider-revolution-responsive-wordpress-plugin/',
        ),
        
        
        array(
            'name'               => 'Netbase Printshop Widgets for SiteOrigin',
            'slug'               => 'netbase-widgets-for-siteorigin-print', 
            'source'             => esc_url('http://demo9.cmsmart.net/plugins/printshop-solution/netbase-widgets-for-siteorigin-print.zip'),
            'required'           => false,
            'force_activation'   => false,
            'force_deactivation' => false,            
        ),
        
        array(
            'name'               => 'So widgets bundle',
            'slug'               => 'so-widgets-bundle', 
            'required'           => true,
            'force_activation'   => false,
            'force_deactivation' => false,    
        ),

        array(
            'name'               => 'Netbase Shortcodes',
            'slug'               => 'netbase-shortcodes', 
            'source'             => esc_url('http://netbaseteam.com/wordpress/theme/plugins/printshop-solution/netbase-shortcodes.zip'),
            'required'           => false,
            'force_activation'   => false,
            'force_deactivation' => false,            
        ),

        array(
            'name'               => esc_html__('Dokan for Netbase Solutions', 'printshop'),
            'slug'               => 'dokan-netbase-solutions', 
            'source'             => esc_url('http://demo9.cmsmart.net/plugins/printshop-solution/dokan-netbase-solutions.zip'),
            'required'           => false,
            'force_activation'   => false,
            'force_deactivation' => false,            
        ),    
        array(
            'name'               => 'Nbdesigner',
            'slug'               => 'web-to-print-online-designer', 
            'required'           => false,
            'force_activation'   => false,
            'force_deactivation' => false,
            'source'             => esc_url('http://demo9.cmsmart.net/plugins/printshop-solution/web-to-print-online-designer.zip')
        ),
        
        array(
            'name'              => 'Breadcrumb NavXT',
            'slug'              => 'breadcrumb-navxt', 
            'required'          => false,
            'force_activation'  => false,
        ),

        array(
            'name'              => 'WooCommerce',
            'slug'              => 'woocommerce', 
            'required'          => false,
            'force_activation'  => false,
        ),

        array(
            'name'              => 'YITH WooCommerce Compare',
            'slug'              => 'yith-woocommerce-compare', 
            'required'          => false,
            'force_activation'  => false,
        ),

        array(
            'name'              => 'YITH WooCommerce Wishlist',
            'slug'              => 'yith-woocommerce-wishlist', 
            'required'          => false,
            'force_activation'  => false,
        ),

        array(
            'name'              => 'YITH WooCommerce Quick View',
            'slug'              => 'yith-woocommerce-quick-view', 
            'required'          => false,
            'force_activation'  => false,
        ),

        array(
            'name'              => 'Contact Form 7',
            'slug'              => 'contact-form-7', 
            'required'          => false,
            'force_activation'  => false,
        ),
        array(
            'name'              => 'ThirstyAffiliates',
            'slug'              => 'thirstyaffiliates', 
            'required'          => false,
            'force_activation'  => false,
        ),
        array(
            'name'              => 'WooCommerce Coupon Generator',
            'slug'              => 'coupon-generator-for-woocommerce', 
            'required'          => false,
            'force_activation'  => false,
            'source'             => esc_url('http://netbaseteam.com/wordpress/theme/plugins/printshop-solution/coupon-generator-for-woocommerce.zip'),
        ),
        array(
            'name'              => 'Max Mega Menu',
            'slug'              => 'megamenu', 
            'required'          => false,
            'force_activation'  => false,
        ),

        array(
            'name'              => 'Yoast SEO',
            'slug'              => 'wordpress-seo', 
            'required'          => false,
            'force_activation'  => false,
        ),
        array(
            'name'              => 'Max Mega Menu pro',
            'slug'              => 'megamenu-pro', 
            'required'          => false,
            'force_activation'  => false,
            'source'             => esc_url('http://demo7.cmsmart.net/wordpress/printshop_solution/updateplugins/megamenu-pro.zip')
        ),
        array(
            'name'              => 'Netbase Solutions',
            'slug'              => 'netbase-solutions', 
            'required'          => true,
            'force_activation'  => false,
            'source'             => esc_url('http://demo9.cmsmart.net/plugins/printshop-solution/netbase-solutions.zip')
        ),
    );

    /**
     * Array of configuration settings. Amend each line as needed.
     * If you want the default strings to be available under your own theme domain,
     * leave the strings uncommented.
     * Some of the strings are added into a sprintf, so see the comments at the
     * end of each line for what each argument will be.
     */
    $config = array(
        'default_path' => '',                      // Default absolute path to pre-packaged plugins.
        'menu'         => 'tgmpa-install-plugins', // Menu slug.
        'has_notices'  => true,                    // Show admin notices or not.
        'dismissable'  => true,                    // If false, a user cannot dismiss the nag message.
        'dismiss_msg'  => '',                      // If 'dismissable' is false, this message will be output at top of nag.
        'is_automatic' => false,                   // Automatically activate plugins after installation or not.
        'message'      => '',                      // Message to output right before the plugins table.
        'strings'      => array(
            'page_title'                      => esc_html__( 'Install Required Plugins', 'wpnetbase' ),
            'menu_title'                      => esc_html__( 'Install Plugins', 'wpnetbase' ),
            'installing'                      => esc_html__( 'Installing Plugin: %s', 'wpnetbase' ), // %s = plugin name.
            'oops'                            => esc_html__( 'Something went wrong with the plugin API.', 'wpnetbase' ),
            'notice_can_install_required'     => _n_noop( 'This theme requires the following plugin: %1$s.', 'This theme requires the following plugins: %1$s.', 'wpnetbase'  ), // %1$s = plugin name(s).
            'notice_can_install_recommended'  => _n_noop( 'This theme recommends the following plugin: %1$s.', 'This theme recommends the following plugins: %1$s.', 'wpnetbase'  ), // %1$s = plugin name(s).
            'notice_cannot_install'           => _n_noop( 'Sorry, but you do not have the correct permissions to install the %s plugin. Contact the administrator of this site for help on getting the plugin installed.', 'Sorry, but you do not have the correct permissions to install the %s plugins. Contact the administrator of this site for help on getting the plugins installed.', 'wpnetbase'  ), // %1$s = plugin name(s).
            'notice_can_activate_required'    => _n_noop( 'The following required plugin is currently inactive: %1$s.', 'The following required plugins are currently inactive: %1$s.', 'wpnetbase'   ), // %1$s = plugin name(s).
            'notice_can_activate_recommended' => _n_noop( 'The following recommended plugin is currently inactive: %1$s.', 'The following recommended plugins are currently inactive: %1$s.', 'wpnetbase'   ), // %1$s = plugin name(s).
            'notice_cannot_activate'          => _n_noop( 'Sorry, but you do not have the correct permissions to activate the %s plugin. Contact the administrator of this site for help on getting the plugin activated.', 'Sorry, but you do not have the correct permissions to activate the %s plugins. Contact the administrator of this site for help on getting the plugins activated.', 'wpnetbase'  ), // %1$s = plugin name(s).
            'notice_ask_to_update'            => _n_noop( 'The following plugin needs to be updated to its latest version to ensure maximum compatibility with this theme: %1$s.', 'The following plugins need to be updated to their latest version to ensure maximum compatibility with this theme: %1$s.', 'wpnetbase'  ), // %1$s = plugin name(s).
            'notice_cannot_update'            => _n_noop( 'Sorry, but you do not have the correct permissions to update the %s plugin. Contact the administrator of this site for help on getting the plugin updated.', 'Sorry, but you do not have the correct permissions to update the %s plugins. Contact the administrator of this site for help on getting the plugins updated.', 'wpnetbase'  ), // %1$s = plugin name(s).
            'install_link'                    => _n_noop( 'Begin installing plugin', 'Begin installing plugins', 'wpnetbase'  ),
            'activate_link'                   => _n_noop( 'Begin activating plugin', 'Begin activating plugins', 'wpnetbase'  ),
            'return'                          => esc_html__( 'Return to Required Plugins Installer', 'wpnetbase' ),
            'plugin_activated'                => esc_html__( 'Plugin activated successfully.', 'wpnetbase' ),
            'complete'                        => esc_html__( 'All plugins installed and activated successfully. %s', 'wpnetbase' ), // %s = dashboard link.
            'nag_type'                        => 'updated' // Determines admin notice type - can only be 'updated', 'update-nag' or 'error'.
        )
    );

    tgmpa( $plugins, $config );

}